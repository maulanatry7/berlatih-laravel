@extends('adminlte.master')
@section('content')
    <section class="content">
        <div class="mt-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New Cast</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/cast" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" class="form-control">
                    @error('nama')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror  
                </div>
                  <div class="form-group">
                    <label >Umur</label>
                    <input type="number" name="umur" value="{{old('umur','')}}" class="form-control">
                    @error('umur')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label >Bio</label>
                    <input type="text" name="bio" class="form-control">
                    @error('bio')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        </div>
    </section>
@endsection