@extends('adminlte.master')
@section('content')
    <section class="content">
        <div class="mt-3">
        <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Cast</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                {{session('success')}}
                </div>
              @endif
                <a href="/cast/create" class="btn btn-primary mb-2">Create new cast</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Nama</th>
                      <th>Umur</th>
                      <th>Bio</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($casts as $key => $cast)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$cast->nama}}</td>
                        <td>{{$cast->umur}}</td>
                        <td>{{$cast->bio}}</td>
                        <td style="display:flex;">
                            <a href="cast/{{$cast->id}}" class="btn btn-info btn-xs">Show</a>
                            <a href="cast/{{$cast->id}}/edit" class="btn btn-default btn-xs">Edit</a>
                            <form action="/cast/{{$cast->id}}" method="post">
                            @csrf
                            @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-xs" >
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5" align="center" >No Data Cast</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
        </div> 
    </section>
@endsection