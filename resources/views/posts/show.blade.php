@extends('adminlte.master')
@section('content')
    <section class="content">
        <div class="mt-3">
        <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Cast</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <p>
                    Nama : {{$cast->nama}} <br>
                    Umur : {{$cast->umur}} <br>
                    Bio : {{$cast->bio}}
                </p>
              </div>
              <!-- /.card-body -->
            </div>
        </div> 
    </section>
@endsection