@extends('adminlte.master')
@section('content')
    <section class="content">
    <div class="card">
            <div class="card-header">
            <h3 class="card-title">Register</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
                </button>
            </div>
            </div>
            <div class="card-body">            
            <form action="/welcome" method="post">
                @csrf
                <h1>Buat Account Baru !</h1>
                <h2>Sign Up</h2>
                <label>First name :</label><br><br>
                <input name="nama_awal" type="text"><br><br>
                <label>Last name :</label><br><br>
                <input name="nama_akhir" type="text"><br><br>
                <label>Gender :</label><br><br>
                <input type="radio" name="gender" value="male"> Male <br>
                <input type="radio" name="gender" value = "female"> Female <br>
                <input type="radio" name="gender" value ="other"> Other <br><br>
                <label>Nationality :</label><br><br>
                <select name="nationality">
                    <option value="indonesia">Indonesia</option>
                    <option value="english">English</option>
                    <option value="other">Other</option>
                </select><br><br>
                <label>Language Spoken</label><br><br>
                <input type="checkbox" name="language" value = "b.indo"> Bahasa Indonesia <br>
                <input type="checkbox" name="language" value="english"> English <br>
                <input type="checkbox" name="language"value="other"> Other <br><br>
                <label>Bio :</label><br>
                <textarea name="bio" cols="30" rows="10"></textarea><br>
                <input type="submit">
            </form>
            </div>
            <!-- /.card-body -->
            
            <!-- /.card-footer-->
        </div>
    </section>
@endsection
