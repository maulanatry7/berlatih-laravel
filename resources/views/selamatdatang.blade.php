@extends('adminlte.master')
@section('content')
    <section class="content">
    <div class="card">
            <div class="card-header">
            <h3 class="card-title">Welcome !</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
                </button>
            </div>
            </div>
            <div class="card-body">            
                <h1>SELAMAT DATANG !</h1>
                <h2>{{$nama_dpn}} {{$akhr}}</h2>
                <h2>Terima kasih telah bergabung di SanberBook. Social Media kita bersama !</h2>
            </div>
            <!-- /.card-body -->
            
            <!-- /.card-footer-->
        </div>
    </section>
@endsection
