@extends('adminlte.master')
@section('content')
    <section class="content">
    <div class="card">
            <div class="card-header">
            <h3 class="card-title">Home</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
                </button>
            </div>
            </div>
            <div class="card-body">            
                <h1>SanberBook</h1>
                <h2>Social Media Developer Santai Berkualitas</h2>
                <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
                <h3>Benefit Join di SanberBook</h3>
                <ul>
                    <li>Mendapat motivasi dari sesama developer</li>
                    <li>Sharing knowledge dari para mastah Sanber</li>
                    <li>Dibuat oleh calon web developer terbaik</li>
                </ul>
                <h4>Cara bergabung ke SanberBook</h4>
                <ol>
                    <li>Mengunjungi website ini</li>
                    <li>Mendaftar di <a href="/register"> Form Sign Up</a></li>
                    <li>Selesai !</li>
                </ol>
            </div>
            <!-- /.card-body -->
            
            <!-- /.card-footer-->
        </div>
    </section>
@endsection