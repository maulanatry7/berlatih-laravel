<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }
    public function welcome_post(Request $request){
        $nama_dpn = $request["nama_awal"]; 
        $nama_blkg = $request["nama_akhir"]; 
        return view('selamatdatang',["nama_dpn" =>$nama_dpn, "akhr" => $nama_blkg]);
    }
}