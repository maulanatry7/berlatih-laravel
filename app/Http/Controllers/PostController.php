<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    public function create(){
        return view('posts.create');
    }
    public function store( Request $request){
         //dd($request->all());
         $request->validate([
             'nama' => 'required',
             'umur' => 'required',
             'bio' => 'required'
         ]);
         $query = DB::table('cast')->insert([
             "nama" => $request["nama"],
             "umur" => $request["umur"],
             "bio" => $request["bio"]
         ]);
         return redirect('/cast')->with('success', 'Cast berhasil di simpan !');
    }
    public function index(){
        $casts = DB::table('cast')->get();
        //dd($cast);
        return view('posts.index', compact('casts'));
    }

    public function show($id){
        $cast=DB::table('cast')->where('id',$id)->first();
        return view('posts.show', compact('cast'));
    }

    public function edit($id){
        $cast=DB::table('cast')->where('id',$id)->first();
        return view('posts.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')
                    ->where('id', $id)
                    ->update([
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio']
                    ]);
        return redirect('/cast')->with('success', 'Cast berhasil di update !');
    }

    public function destroy($id){
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with('success', 'Cast berhasil di hapus !');
    }
}
