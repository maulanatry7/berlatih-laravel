<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@form');
Route::post('/welcome', 'AuthController@welcome_post');
Route::get('/master', function(){
    return view('adminlte/master');
});
Route::get('/items', function(){
    return view('items/index');
});
Route::get('/table', function(){
    return view('table');
});
Route::get('/data-tables', function(){
    return view('data_table');
});
Route::get('/cast/create', 'PostController@create');
Route::post('/cast', 'PostController@store');
Route::get('/cast', 'PostController@index');
Route::get('/cast/{id}', 'PostController@show');
Route::get('/cast/{id}/edit', 'PostController@edit');
Route::put('/cast/{id}', 'PostController@update');
Route::delete('/cast/{id}', 'PostController@destroy');


